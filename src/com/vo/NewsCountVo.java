package com.vo;

import java.util.List;

import com.entity.Articletype;
import com.entity.News;
import com.entity.Newstype;

/**
 * NewsCountVo.java
 * @author  microxdd
 * @version 创建时间：2014 2014年9月17日 下午11:40:33 
 * micrxdd
 * 
 */
public class NewsCountVo {
    private Articletype articletype;
    private List<News> news;
    public Articletype getArticletype() {
        return articletype;
    }

    public void setArticletype(Articletype articletype) {
        this.articletype = articletype;
    }

    public List<News> getNews() {
        return news;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }
    
}

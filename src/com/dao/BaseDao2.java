package com.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;

public interface BaseDao2 {
	/**
     * 保存对象
	 * @param <T>
     * @param entity
     */
    public <T> void save(T entity);
    /**
     * 删除一个对象
     * @param <T>
     * @param entity
     */
    public <T> void del(T entity);
    /**
     * 更新一个对象
     * @param <T>
     * @param entity
     */
    public <T> void update(T entity);
    /**
     * 通过id查找一个对象
     * @param <T>
     * @param id
     * @return
     */
    public <T> T findById(Class<T> clazz,int id);
    /**
     * 查找所有对象
     * @param <T>
     * @return
     */
    public <T> List<T> findAll(Class<T> clazz,Criterion... pamas);
}

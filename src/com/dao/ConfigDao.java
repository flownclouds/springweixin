package com.dao;

import com.entity.Config;

public interface ConfigDao extends BaseDao<Config>{
    /**
     * 查找配置
     * @param key
     * @return
     */
    public Config findone(String key);
}

package com.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Articletype entity. @author MyEclipse Persistence Tools
 */

public class Articletype implements java.io.Serializable {

    // Fields

    private Integer id;
    private Articletype articletype;
    private String name;
    private String description;
    private Set articletypes = new HashSet(0);
    private Set articles = new HashSet(0);

    // Constructors

    /** default constructor */
    public Articletype() {
    }

    /** full constructor */
    public Articletype(Articletype articletype, String name,
	    String description, Set articletypes, Set articles) {
	this.articletype = articletype;
	this.name = name;
	this.description = description;
	this.articletypes = articletypes;
	this.articles = articles;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Articletype getArticletype() {
	return this.articletype;
    }

    public void setArticletype(Articletype articletype) {
	this.articletype = articletype;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getDescription() {
	return this.description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Set getArticletypes() {
	return this.articletypes;
    }

    public void setArticletypes(Set articletypes) {
	this.articletypes = articletypes;
    }

    public Set getArticles() {
	return this.articles;
    }

    public void setArticles(Set articles) {
	this.articles = articles;
    }

}
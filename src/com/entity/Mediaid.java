package com.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Mediaid entity. @author MyEclipse Persistence Tools
 */

public class Mediaid implements java.io.Serializable {

    // Fields

    private Integer id;
    private String mediaid;
    private Timestamp ctime;
    private Integer mediaType;
    private Set videoses = new HashSet(0);
    private Set imageses = new HashSet(0);
    private Set voiceses = new HashSet(0);

    // Constructors

    /** default constructor */
    public Mediaid() {
    }

    /** minimal constructor */
    public Mediaid(String mediaid) {
	this.mediaid = mediaid;
    }

    /** full constructor */
    public Mediaid(String mediaid, Timestamp ctime, Integer mediaType,
	    Set videoses, Set imageses, Set voiceses) {
	this.mediaid = mediaid;
	this.ctime = ctime;
	this.mediaType = mediaType;
	this.videoses = videoses;
	this.imageses = imageses;
	this.voiceses = voiceses;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getMediaid() {
	return this.mediaid;
    }

    public void setMediaid(String mediaid) {
	this.mediaid = mediaid;
    }

    public Timestamp getCtime() {
	return this.ctime;
    }

    public void setCtime(Timestamp ctime) {
	this.ctime = ctime;
    }

    public Integer getMediaType() {
	return this.mediaType;
    }

    public void setMediaType(Integer mediaType) {
	this.mediaType = mediaType;
    }

    public Set getVideoses() {
	return this.videoses;
    }

    public void setVideoses(Set videoses) {
	this.videoses = videoses;
    }

    public Set getImageses() {
	return this.imageses;
    }

    public void setImageses(Set imageses) {
	this.imageses = imageses;
    }

    public Set getVoiceses() {
	return this.voiceses;
    }

    public void setVoiceses(Set voiceses) {
	this.voiceses = voiceses;
    }

}
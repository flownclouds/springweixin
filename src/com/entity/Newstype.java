package com.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Newstype entity. @author MyEclipse Persistence Tools
 */

public class Newstype implements java.io.Serializable {

    // Fields

    private Integer id;
    private Newstype newstype;
    private String text;
    private Set newstypes = new HashSet(0);
    private Set newsitems = new HashSet(0);

    // Constructors

    /** default constructor */
    public Newstype() {
    }

    /** full constructor */
    public Newstype(Newstype newstype, String text, Set newstypes, Set newsitems) {
	this.newstype = newstype;
	this.text = text;
	this.newstypes = newstypes;
	this.newsitems = newsitems;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Newstype getNewstype() {
	return this.newstype;
    }

    public void setNewstype(Newstype newstype) {
	this.newstype = newstype;
    }

    public String getText() {
	return this.text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public Set getNewstypes() {
	return this.newstypes;
    }

    public void setNewstypes(Set newstypes) {
	this.newstypes = newstypes;
    }

    public Set getNewsitems() {
	return this.newsitems;
    }

    public void setNewsitems(Set newsitems) {
	this.newsitems = newsitems;
    }

}
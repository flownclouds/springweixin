package com.entity;

/**
 * News entity. @author MyEclipse Persistence Tools
 */

public class News implements java.io.Serializable {

    // Fields

    private Integer id;
    private Newsitem newsitem;
    private Article article;
    private String title;
    private String description;
    private String picUrl;
    private String url;
    private Integer articleid;
    private Integer articletypeid;

    // Constructors

    /** default constructor */
    public News() {
    }

    /** minimal constructor */
    public News(String title) {
	this.title = title;
    }

    /** full constructor */
    public News(Newsitem newsitem, Article article, String title,
	    String description, String picUrl, String url, Integer articleid,
	    Integer articletypeid) {
	this.newsitem = newsitem;
	this.article = article;
	this.title = title;
	this.description = description;
	this.picUrl = picUrl;
	this.url = url;
	this.articleid = articleid;
	this.articletypeid = articletypeid;
	this.article = article;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Newsitem getNewsitem() {
	return this.newsitem;
    }

    public void setNewsitem(Newsitem newsitem) {
	this.newsitem = newsitem;
    }

    public Article getArticle() {
	return this.article;
    }

    public void setArticle(Article article) {
	this.article = article;
    }

    public String getTitle() {
	return this.title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getDescription() {
	return this.description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getPicUrl() {
	return this.picUrl;
    }

    public void setPicUrl(String picUrl) {
	this.picUrl = picUrl;
    }

    public String getUrl() {
	return this.url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public Integer getArticleid() {
	return this.articleid;
    }

    public void setArticleid(Integer articleid) {
	this.articleid = articleid;
    }

    public Integer getArticletypeid() {
        return articletypeid;
    }

    public void setArticletypeid(Integer articletypeid) {
        this.articletypeid = articletypeid;
    }
    

}
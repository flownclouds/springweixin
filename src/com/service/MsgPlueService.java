package com.service;

public interface MsgPlueService {
	public <T> T getPlugin(Class<T> t);
	public Object getPlugin(String name);
}

package com.dto;

import java.sql.Timestamp;
public class NewsItemDto {
    private Integer id;
    private String username;
    private Timestamp ctime;
    private String description;
    private Integer typeid;
    
    public Integer getTypeid() {
        return typeid;
    }
    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public Timestamp getCtime() {
        return ctime;
    }
    public void setCtime(Timestamp ctime) {
        this.ctime = ctime;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
}

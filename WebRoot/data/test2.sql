/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.15-log : Database - weixin
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `article` */

DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `isshow` tinyint(1) DEFAULT '1',
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD458CCF6948BCBB4` (`author`),
  KEY `FK_article_id` (`type_id`),
  CONSTRAINT `FKD458CCF6948BCBB4` FOREIGN KEY (`author`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_article_id` FOREIGN KEY (`type_id`) REFERENCES `articletype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

/*Data for the table `article` */

insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (1,1,'你好这是title','2014-09-13 23:51:57',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (2,1,'aaaaaaaa','2014-09-16 22:45:28',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (3,1,'ssss','2014-09-09 23:40:18',1,NULL);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (4,1,'aaaa','2014-09-09 23:43:25',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (5,1,'aaaa','2014-09-09 23:47:04',1,4);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (6,1,'aaaa','2014-09-10 20:40:51',1,4);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (7,1,'aaaa','2014-09-13 12:03:22',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (8,1,'文章测试','2014-09-13 12:04:05',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (9,1,'这是文章测试哈哈','2014-09-13 12:06:59',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (10,1,'11这是文章测试哈哈','2014-09-13 12:15:21',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (11,1,'22这是文章测试哈哈','2014-09-13 12:17:45',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (12,1,'12的修改','2014-09-13 13:51:02',1,4);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (13,1,'22这是文章测试哈哈','2014-09-13 13:03:05',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (14,1,'22这是文章测试哈哈','2014-09-13 13:03:53',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (32,1,'aaaa','2014-09-16 20:35:48',1,4);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (38,1,'aaaaaaaa','2014-09-16 22:32:11',1,1);
insert  into `article`(`id`,`author`,`title`,`ctime`,`isshow`,`type_id`) values (47,1,'视频测试','2014-09-17 00:46:36',1,1);

/*Table structure for table `articletype` */

DROP TABLE IF EXISTS `articletype`;

CREATE TABLE `articletype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_articletype_pid` (`pid`),
  CONSTRAINT `FK_articletype_pid` FOREIGN KEY (`pid`) REFERENCES `articletype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `articletype` */

insert  into `articletype`(`id`,`name`,`description`,`pid`) values (1,'分类11','1',NULL);
insert  into `articletype`(`id`,`name`,`description`,`pid`) values (2,'分类2','2',NULL);
insert  into `articletype`(`id`,`name`,`description`,`pid`) values (3,'分类3','3',NULL);
insert  into `articletype`(`id`,`name`,`description`,`pid`) values (4,'分类1-1','4',1);
insert  into `articletype`(`id`,`name`,`description`,`pid`) values (5,'分类2-1','5',2);
insert  into `articletype`(`id`,`name`,`description`,`pid`) values (6,'分类3-1','6',3);

/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `website` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `config` */

/*Table structure for table `event` */

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(10) NOT NULL,
  `descri` varchar(200) DEFAULT NULL,
  `retype` varchar(10) DEFAULT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `eventkey` varchar(10) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `plugin` varchar(20) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `event` */

insert  into `event`(`id`,`event`,`descri`,`retype`,`msg_id`,`eventkey`,`ctime`,`plugin`,`enable`) values (1,'subscribe',NULL,'news',1,NULL,NULL,NULL,0);
insert  into `event`(`id`,`event`,`descri`,`retype`,`msg_id`,`eventkey`,`ctime`,`plugin`,`enable`) values (2,'CLICK',NULL,'news',2,'ttt',NULL,NULL,0);

/*Table structure for table `html` */

DROP TABLE IF EXISTS `html`;

CREATE TABLE `html` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `html` text,
  `article_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_id` (`article_id`),
  KEY `FK3107AB73002DC` (`article_id`),
  CONSTRAINT `FK3107AB73002DC` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

/*Data for the table `html` */

insert  into `html`(`id`,`html`,`article_id`) values (1,'<p>你好这是一个测试<img src=\"/wei/upload/image/20140913/1410623300321014471.jpg\" title=\"1410623300321014471.jpg\" alt=\"QQ截图20140913234814.jpg\"/></p>',1);
insert  into `html`(`id`,`html`,`article_id`) values (2,'<p>aaaaaaa</p>',2);
insert  into `html`(`id`,`html`,`article_id`) values (3,'sdxsds',3);
insert  into `html`(`id`,`html`,`article_id`) values (4,'sxsxsxs',4);
insert  into `html`(`id`,`html`,`article_id`) values (5,'sxsxs',5);
insert  into `html`(`id`,`html`,`article_id`) values (6,'<p>你好啊<br/></p>',6);
insert  into `html`(`id`,`html`,`article_id`) values (7,'<p>你好 这是文章的测试</p>',7);
insert  into `html`(`id`,`html`,`article_id`) values (8,'<p>你好 这是文章的测试哈哈</p>',8);
insert  into `html`(`id`,`html`,`article_id`) values (9,'<p>11你好 这是文章的测试哈哈</p>',9);
insert  into `html`(`id`,`html`,`article_id`) values (10,'<p>22你好 这是文章的测试哈哈</p>',10);
insert  into `html`(`id`,`html`,`article_id`) values (11,'这是111',11);
insert  into `html`(`id`,`html`,`article_id`) values (12,'<p>12的修改</p>',12);
insert  into `html`(`id`,`html`,`article_id`) values (13,'<p>这是2222222</p>',13);
insert  into `html`(`id`,`html`,`article_id`) values (14,'<p>这是111</p>',14);
insert  into `html`(`id`,`html`,`article_id`) values (32,'<p>aaaa</p>',32);
insert  into `html`(`id`,`html`,`article_id`) values (38,'<p>aaaaaaa</p>',38);
insert  into `html`(`id`,`html`,`article_id`) values (41,'<p><video class=\"video\" poster=\"http://media.html5media.info/poster.jpg\" width=\"100%\" controls=\"\" preload=\"\"><source src=\"http://media.html5media.info/video.mp4\" media=\"only screen and (min-device-width: 960px)\"/><source src=\"http://media.html5media.info/video.iphone.mp4\" media=\"only screen and (max-device-width: 960px)\"/><source src=\"http://media.html5media.info/video.ogv\"/></video></p><p>你好啊x</p><p>那时候刚好下着雨，柏油路面湿冷冷的，还闪烁着青、黄、红颜色的灯火。我们就在骑楼下躲雨，看绿色的邮筒孤独地站在街的对面。我白色风衣的大口袋里有一封要寄给南部的母亲的信。樱子说她可以撑伞过去帮我寄信。我默默点头。</p><p>“谁叫我们只带来一把小伞哪。”她微笑着说，一面撑起伞，准备过马路帮我寄信。从她伞骨渗下来的小雨点，溅在我的眼镜玻璃上。</p><p>随着一阵拔尖的煞车声，樱子的一生轻轻地飞了起来。缓缓地，飘落在湿冷的街面上，好像一只夜晚的蝴蝶。</p><p>虽然是春天，好像已是秋深了。</p><p><img src=\"http://s.cn.bing.net/az/hprichbg/rb/DongBaSymbols_ZH-CN10223363633_1366x768.jpg\" alt=\"\" width=\"240\"/></p><p>她只是过马路去帮我寄信。这简单的行动，却要叫我终身难忘了。我缓缓睁开眼，茫然站在骑楼下，眼里裹着滚烫的泪水。世上所有的车子都停了下来，人潮涌向马路中央。没有人知道那躺在街面的，就是我的，蝴蝶。这时她只离我五公尺，竟是那么遥远。更大的雨点溅在我的眼镜上，溅到我的生命里来。</p><p>为什么呢？只带一把雨伞？</p><p>然而我又看到樱子穿着白色的风衣，撑着伞，静静地过马路了。她是要帮我寄信的。那，那是一封写给南部母亲的信。我茫然站在骑楼下，我又看到永远的樱子走到街心。其实雨下得并不大，却是一生一世中最大的一场雨。而那封信是这样写的，年轻的樱子知不知道呢？</p><blockquote>妈：我打算在下个月和樱子结婚。</blockquote><hr class=\"am-article-divider\"/><h2>作者简介</h2><p>《永远的蝴蝶》作者陈启佑，笔名渡也、江山之助，台湾省嘉义市人，中国文化大学中国文学博士，曾任教于嘉义农专、台湾教育学院。</p><hr class=\"am-article-divider\"/><h2>文章赏析</h2><h3>赏析一</h3><p>《永远的蝴蝶》就像一支低沉而哀怨的悲曲，幽幽道来，委婉动人。读罢此文，脑海中一直浮现着这样一个场景：阴雨中，樱子如蝴蝶般轻轻飞了起来，又缓缓落到了街面上。雨是冰凉的，街面是湿冷的，蝴蝶的飘飞是凄美的，又是令人伤痛的。</p><h3>赏析二</h3><p>文章有几点尤值得我们好好品味。一是作品以“雨”为线索，贯穿全文的始终。悲剧因“雨”而生，小说开篇写“雨”，正是对不幸和灾难起因的一个交代。樱子遭遇不幸后，又写“更大的雨点溅在我的眼镜上，溅到我的生命里来”，“成为一生一世的一场雨”。显然，“雨”又成为泪水和痛苦的象征。同时，以“雨”贯穿全文，也造成笼罩全文的阴冷凄凉的氛围。二是作家善于反复运用细节。如三次写到“站在骑楼下”，以此使“我”的情感思绪变化的脉络和层次更加清楚明显；两次写到樱子“穿着白色的风衣，撑着伞”，这是对“我”的心理刻画，突出了樱子美丽清纯的形象，也表达了“我”对樱子永不磨灭的爱。三是高超的谋篇布局技巧。直到作品的结尾处才告诉读者信的内容，这样构思，无疑加重了作品的悲剧色彩，让人哀痛欲绝，心不堪受。正因为作家善于谋篇布局，匠心独运，作品才有了很强的感染力。</p><p><small>朗读并背诵全文</small></p>',47);

/*Table structure for table `images` */

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `auther` int(11) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB95A8278948BCA7E` (`auther`),
  KEY `FKB95A82782B649837` (`media_id`),
  CONSTRAINT `FKB95A82782B649837` FOREIGN KEY (`media_id`) REFERENCES `mediaid` (`id`),
  CONSTRAINT `FKB95A8278948BCA7E` FOREIGN KEY (`auther`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `images` */

/*Table structure for table `mediaid` */

DROP TABLE IF EXISTS `mediaid`;

CREATE TABLE `mediaid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mediaid` varchar(65) NOT NULL,
  `ctime` datetime DEFAULT NULL,
  `media_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `mediaid` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `text` varchar(20) NOT NULL,
  `iconCls` varchar(10) DEFAULT NULL,
  `checked` tinyint(1) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `st` tinyint(1) DEFAULT '1',
  `description` varchar(100) DEFAULT NULL,
  `action` tinyint(1) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK33155FE85C8548` (`pid`),
  CONSTRAINT `FK33155FE85C8548` FOREIGN KEY (`pid`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (1,NULL,'微信消息管理','icon-save',NULL,'aa',1,'aaa',0,'admin');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (6,NULL,'系统设置',NULL,NULL,NULL,1,NULL,0,'admin');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (7,6,'菜单管理','icon-save',NULL,'role.html',1,'管理菜单',0,'admin:role');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (27,1,'文本消息管理','icon-save',NULL,'textmsg.html',0,'管理文本消息',0,'admin:textmsg');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (28,1,'新闻消息管理','icon-save',NULL,'newsmsg.html',0,'管理新闻消息',0,'admin:newsmsg');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (29,NULL,'用户管理','icon-save',NULL,'user.html',0,'管理用户',0,'admin');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (31,29,'用户管理','icon-save',NULL,'user.html',0,'管理用户',0,'admin:user');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (32,29,'用户组管理','icon-save',NULL,'usergroup.html',0,'管理用户组',0,'admin:usergroup');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (33,1,'消息分发管理','icon-save',NULL,'msgprs.html',0,'消息分发',0,'admin:msgprs');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (34,1,'消息回复规则','icon-save',NULL,'msgrole.html',0,'消息规则定制',0,'admin:msgrole');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (38,1,'文本关键字定制','icon-save',NULL,'textrole.html',0,'文本关键字东芝',0,'admin:textrole');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (39,1,'微信菜单管理','icon-save',NULL,'weixinmenus.html',0,'微信菜单管理',0,'admin:weixinmenus');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (43,NULL,'文章管理','icon-save',NULL,'null',0,'文章管理',0,'admin');
insert  into `menu`(`id`,`pid`,`text`,`iconCls`,`checked`,`url`,`st`,`description`,`action`,`role`) values (44,43,'文章管理','icon-save',NULL,'article.html',0,'管理文章',0,'admin:article');

/*Table structure for table `msgprs` */

DROP TABLE IF EXISTS `msgprs`;

CREATE TABLE `msgprs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `retype` varchar(10) DEFAULT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `stime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `plugin` varchar(20) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `msgprs` */

insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (2,'返回的是文本消息','text',30,'2014-09-08 14:58:03','2014-09-30 15:00:15','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (3,'返回新闻','news',NULL,'2014-09-16 14:11:56','2014-09-30 00:00:00','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (4,'语音消息处理器','text',31,'2014-09-16 15:02:49','2016-12-31 00:00:00','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (5,'图片消息处理器','text',33,'2014-09-16 15:04:53','2014-11-27 00:00:00','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (6,'位置消息处理器','text',34,'2014-09-16 15:11:06','2014-12-31 00:00:00','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (7,'用户关注后','text',35,'2014-09-16 15:11:50','2015-01-01 00:00:00','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (8,'用户取消关注','text',36,'2014-09-16 15:16:50','2015-01-28 00:00:00','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (9,'菜单事件','text',37,'2014-09-16 15:17:33','2015-01-28 00:00:00','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (10,'处理链接消息','text',38,'2014-09-16 15:26:33','2015-01-01 00:00:00','',0);
insert  into `msgprs`(`id`,`name`,`retype`,`msg_id`,`stime`,`endtime`,`plugin`,`enable`) values (11,'处理视频','text',32,'2014-09-16 15:33:34','2015-01-01 00:00:00','',0);

/*Table structure for table `msgrole` */

DROP TABLE IF EXISTS `msgrole`;

CREATE TABLE `msgrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rtype` varchar(50) DEFAULT NULL,
  `event` varchar(50) DEFAULT NULL,
  `eventkey` varchar(50) DEFAULT NULL,
  `msgprsid` int(11) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_msgrole` (`msgprsid`),
  CONSTRAINT `FK_msgrole` FOREIGN KEY (`msgprsid`) REFERENCES `msgprs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `msgrole` */

insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (3,'image','wu','a',5,1);
insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (8,'voice',NULL,'d',4,1);
insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (9,'video','wu','aa',11,1);
insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (10,'location','wu','aaa',6,1);
insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (11,'link','wu','aa',10,1);
insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (12,'event','subscribe','aaaa',3,NULL);
insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (13,'event','subscribe','aa',7,NULL);
insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (14,'event','unsubscribe','aa',8,NULL);
insert  into `msgrole`(`id`,`rtype`,`event`,`eventkey`,`msgprsid`,`enable`) values (15,'event','CLICK','vvv',9,NULL);

/*Table structure for table `music` */

DROP TABLE IF EXISTS `music`;

CREATE TABLE `music` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auther` int(11) DEFAULT NULL,
  `title` varchar(20) NOT NULL,
  `descri` varchar(200) DEFAULT NULL,
  `musicurl` varchar(255) NOT NULL,
  `hqmusicurl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK636EE25948BCA7E` (`auther`),
  CONSTRAINT `FK636EE25948BCA7E` FOREIGN KEY (`auther`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `music` */

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `picUrl` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `newsitem_id` int(11) DEFAULT NULL,
  `articleid` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `articletypeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_id` (`article_id`),
  KEY `FK338AD373002DC` (`article_id`),
  KEY `FK_news` (`newsitem_id`),
  CONSTRAINT `FK338AD373002DC` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  CONSTRAINT `FK_news` FOREIGN KEY (`newsitem_id`) REFERENCES `newsitem` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

/*Data for the table `news` */

insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (83,'这是标题','这个是描述','http://mp.weixin.qq.com/wiki/skins/common/images/weixin_wiki_logo.png',NULL,NULL,1,1,1);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (85,'这是标题2','这个是描述2','http://cn.bing.com/az/hprichv/LondonTrainStation_GettyRR_139321755_ZH-CN742316019.jpg',NULL,NULL,2,2,1);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (86,'这是标题3','这个是描述3','http://s.cn.bing.net/az/hprichbg/rb/CardinalsBerries_ZH-CN10679090179_1366x768.jpg',NULL,NULL,3,3,1);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (87,'这是标题4','这个是描述4','http://s.cn.bing.net/az/hprichbg/rb/QingdaoJiaozhou_ZH-CN10690497202_1366x768.jpg',NULL,NULL,4,4,1);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (88,'这是标题5','这个是描述5','http://s.cn.bing.net/az/hprichbg/rb/FennecFox_ZH-CN13720911949_1366x768.jpg',NULL,NULL,5,5,1);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (89,'这是标题2','这个是描述2','http://img3.douban.com/lpic/o637240.jpg',NULL,NULL,6,6,2);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (90,'这是标题3','这个是描述3','http://img3.douban.com/lpic/o637240.jpg',NULL,NULL,7,7,2);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (91,'这是标题4','这个是描述4','http://img3.douban.com/lpic/o637240.jpg',NULL,NULL,8,8,2);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (92,'这是标题5','这个是描述5','http://s.cn.bing.net/az/hprichbg/rb/FennecFox_ZH-CN13720911949_1366x768.jpg',NULL,NULL,9,9,2);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (93,'这是标题2','这个是描述2','http://img5.douban.com/lpic/o636459.jpg',NULL,NULL,10,10,3);
insert  into `news`(`id`,`title`,`description`,`picUrl`,`url`,`newsitem_id`,`articleid`,`article_id`,`articletypeid`) values (94,'这是标题3','这个是描述3','http://img3.douban.com/lpic/o637240.jpg',NULL,NULL,11,11,3);

/*Table structure for table `newsitem` */

DROP TABLE IF EXISTS `newsitem`;

CREATE TABLE `newsitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auther` int(11) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  `newstype` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_newsitem` (`auther`),
  KEY `FK_newsitem_` (`newstype`),
  CONSTRAINT `FK_newsitem` FOREIGN KEY (`auther`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_newsitem_` FOREIGN KEY (`newstype`) REFERENCES `newstype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `newsitem` */

insert  into `newsitem`(`id`,`auther`,`ctime`,`newstype`,`description`) values (1,1,'2014-08-10 13:09:00',3,'描述');
insert  into `newsitem`(`id`,`auther`,`ctime`,`newstype`,`description`) values (3,1,'2014-08-10 13:38:52',3,'描述');
insert  into `newsitem`(`id`,`auther`,`ctime`,`newstype`,`description`) values (4,1,'2014-08-10 13:40:11',4,'描述');
insert  into `newsitem`(`id`,`auther`,`ctime`,`newstype`,`description`) values (5,1,'2014-08-10 13:45:44',3,'描述');
insert  into `newsitem`(`id`,`auther`,`ctime`,`newstype`,`description`) values (6,1,'2014-09-07 14:25:22',4,'你好');
insert  into `newsitem`(`id`,`auther`,`ctime`,`newstype`,`description`) values (11,1,'2014-08-10 14:10:43',3,'描述');
insert  into `newsitem`(`id`,`auther`,`ctime`,`newstype`,`description`) values (12,1,'2014-09-07 14:29:16',4,'你好啊');
insert  into `newsitem`(`id`,`auther`,`ctime`,`newstype`,`description`) values (15,1,'2014-09-10 00:36:33',14,'这是文章描述');

/*Table structure for table `newstype` */

DROP TABLE IF EXISTS `newstype`;

CREATE TABLE `newstype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(100) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_newstype` (`pid`),
  CONSTRAINT `FK_newstype` FOREIGN KEY (`pid`) REFERENCES `newstype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `newstype` */

insert  into `newstype`(`id`,`text`,`pid`) values (1,'新闻类',NULL);
insert  into `newstype`(`id`,`text`,`pid`) values (2,'安全类',1);
insert  into `newstype`(`id`,`text`,`pid`) values (3,'计算机学院',1);
insert  into `newstype`(`id`,`text`,`pid`) values (4,'商学院',1);
insert  into `newstype`(`id`,`text`,`pid`) values (7,'测试学院,',1);
insert  into `newstype`(`id`,`text`,`pid`) values (14,'AAA',2);
insert  into `newstype`(`id`,`text`,`pid`) values (15,'BBB',2);

/*Table structure for table `plugin` */

DROP TABLE IF EXISTS `plugin`;

CREATE TABLE `plugin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `plugin` */

/*Table structure for table `text` */

DROP TABLE IF EXISTS `text`;

CREATE TABLE `text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `edittime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK36452D948BCBB4` (`author`),
  CONSTRAINT `FK36452D948BCBB4` FOREIGN KEY (`author`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `text` */

insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (30,NULL,'接受到文本  返回文本','2014-09-16 15:00:34','2014-09-16 15:00:34');
insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (31,NULL,'接收到语音','2014-09-16 15:00:34','2014-09-16 15:00:34');
insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (32,NULL,'接收到视频','2014-09-16 15:00:34','2014-09-16 15:00:34');
insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (33,NULL,'接收到图片','2014-09-16 15:00:34','2014-09-16 15:00:34');
insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (34,NULL,'接收到位置','2014-09-16 15:00:34','2014-09-16 15:00:34');
insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (35,NULL,'用户订阅','2014-09-16 15:00:34','2014-09-16 15:00:34');
insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (36,NULL,'用户取消订阅','2014-09-16 15:00:34','2014-09-16 15:00:34');
insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (37,NULL,'菜单时间','2014-09-16 15:00:34','2014-09-16 15:00:34');
insert  into `text`(`id`,`author`,`text`,`time`,`edittime`) values (38,NULL,'链接','2014-09-16 15:26:23','2014-09-16 15:26:23');

/*Table structure for table `textrole` */

DROP TABLE IF EXISTS `textrole`;

CREATE TABLE `textrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txtkey` varchar(10) DEFAULT NULL,
  `msgprsid` int(11) DEFAULT NULL,
  `isstart` tinyint(1) DEFAULT '1',
  `enable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_aaaaa` (`msgprsid`),
  CONSTRAINT `FK_aaaaa` FOREIGN KEY (`msgprsid`) REFERENCES `msgprs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `textrole` */

insert  into `textrole`(`id`,`txtkey`,`msgprsid`,`isstart`,`enable`) values (1,'你好',2,1,1);
insert  into `textrole`(`id`,`txtkey`,`msgprsid`,`isstart`,`enable`) values (2,'你好啊',2,1,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` char(40) NOT NULL,
  `creattime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `FK36EBCB40DF7ABC` (`group_id`),
  CONSTRAINT `FK36EBCB40DF7ABC` FOREIGN KEY (`group_id`) REFERENCES `usersgroup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`group_id`,`username`,`password`,`creattime`) values (1,1,'admin','1e1d21b9e8ae863fe4f3b07297e315b83e8241e1','2014-09-05 21:08:59');
insert  into `user`(`id`,`group_id`,`username`,`password`,`creattime`) values (2,1,'admin1','1e1d21b9e8ae863fe4f3b07297e315b83e8241e1',NULL);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_id` int(11) DEFAULT NULL,
  `menusids` varchar(100) DEFAULT NULL,
  KEY `FK_user_role` (`user_id`),
  KEY `FK_user_role_1` (`menusids`),
  CONSTRAINT `FK_user_role` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

/*Table structure for table `usersgroup` */

DROP TABLE IF EXISTS `usersgroup`;

CREATE TABLE `usersgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(20) NOT NULL,
  `groupdes` varchar(200) DEFAULT NULL,
  `menusids` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `usersgroup` */

insert  into `usersgroup`(`id`,`groupname`,`groupdes`,`menusids`) values (1,'管理员','具有最高权限','1,27,28,33,34,38,39,6,7,29,31,32,43,44,45');

/*Table structure for table `videos` */

DROP TABLE IF EXISTS `videos`;

CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `auther` int(11) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCF527B58948BCA7E` (`auther`),
  KEY `FKCF527B582B649837` (`media_id`),
  CONSTRAINT `FKCF527B582B649837` FOREIGN KEY (`media_id`) REFERENCES `mediaid` (`id`),
  CONSTRAINT `FKCF527B58948BCA7E` FOREIGN KEY (`auther`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `videos` */

/*Table structure for table `voices` */

DROP TABLE IF EXISTS `voices`;

CREATE TABLE `voices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `auther` int(11) DEFAULT NULL,
  `ctime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCFA94581948BCA7E` (`auther`),
  KEY `FKCFA945812B649837` (`media_id`),
  CONSTRAINT `FKCFA945812B649837` FOREIGN KEY (`media_id`) REFERENCES `mediaid` (`id`),
  CONSTRAINT `FKCFA94581948BCA7E` FOREIGN KEY (`auther`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `voices` */

/*Table structure for table `weimenu` */

DROP TABLE IF EXISTS `weimenu`;

CREATE TABLE `weimenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `key` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `msgprsid` int(11) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `FK_weimenu_pid` (`pid`),
  CONSTRAINT `FK_weimenu_pid` FOREIGN KEY (`pid`) REFERENCES `weimenu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `weimenu` */

/*Table structure for table `weixinuser` */

DROP TABLE IF EXISTS `weixinuser`;

CREATE TABLE `weixinuser` (
  `openid` char(30) NOT NULL,
  `nickname` year(4) DEFAULT NULL,
  `sex` int(1) DEFAULT '0',
  `city` varchar(10) DEFAULT NULL,
  `province` varchar(10) DEFAULT NULL,
  `country` varchar(10) DEFAULT NULL,
  `headimgurl` char(255) DEFAULT NULL,
  `subscribe_time` datetime DEFAULT NULL,
  `unionid` char(30) DEFAULT NULL,
  PRIMARY KEY (`openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `weixinuser` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
